import numpy as np
import matplotlib.pyplot as plt

#file=open('data.csv')
data = np.loadtxt('data.csv', delimiter=',', skiprows=1, usecols=(0, 1, 2))
row, col = np.shape(data)

print("Prema velicini polja --> mjerenje je izvrseno na ", row, "osoba")

height_column = data[ : , 1 ]
weight_column = data[ : , 2 ]

plt.scatter(height_column, weight_column, c='m', marker='.')
plt.title('Zadatak2.b')
plt.xlabel('height')
plt.ylabel('weight')
plt.show()

h_column= data[0::50,1]
w_column= data[0::50,2]
plt.scatter(h_column, w_column, c='m', marker='.')
plt.title('Zadatak2.c')
plt.xlabel('height')
plt.ylabel('weight')
plt.show()

max_height_50=max(h_column)
print("Maksimalna visina u skupu od 50 ljudi je: ", max_height_50)

min_height_50=min(h_column)
print("Minimalna visina u skupu od 50 ljudi je: ", min_height_50)

avg_height_50=h_column.mean()
print("Prosjecna visina u skupu od 50 ljudi je: ", avg_height_50)



m_ind = (data[:,0] == 1)
for i in m_ind:
    list_m=np.array(data[1:, 1])


max_height_50_m=max(list_m)
print("Maksimalna visina u skupu od 50 muskaraca je: ", max_height_50_m)
min_height_50_m=min(list_m)
print("Minimalna visina u skupu od 50 muskaraca je: ", min_height_50_m)

avg_height_50_m=list_m.mean()
print("Prosjecna visina u skupu od 50 muskaraca je: ", avg_height_50_m)

z_ind = (data[:,0] == 0)
for i in z_ind:
    list_z=np.array(data[1:, 1])

max_height_50_z=max(list_z)
print("Maksimalna visina u skupu od 50 muskaraca je: ", max_height_50_z)
min_height_50_z=min(list_z)
print("Minimalna visina u skupu od 50 muskaraca je: ", min_height_50_z)

avg_height_50_z=list_z.mean()
print("Prosjecna visina u skupu od 50 muskaraca je: ", avg_height_50_z)



