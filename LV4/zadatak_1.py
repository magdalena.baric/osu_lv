from sklearn import datasets, linear_model
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
import sklearn.linear_model as lm
from sklearn.linear_model import LinearRegression


#ucitaj podatke
data = pd.read_csv('data_C02_emission.csv')

#drom Maks, MOdel
data = data.drop(["Make", "Model"], axis=1)

#kreiraj X i y
input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)', 
                   'Engine Size (L)', 
                   'Cylinders'
                   ]

output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

#for i in range(0,5):
 #   plt.scatter ( X_train[ : , i], y_train, s=2,  c='blue')
  #  plt.scatter ( X_test[ : , i], y_test, s=2, c='red' )
                    #x='CO2 Emissions (g/km)',
                        #    y= 'Cylinders')
   # plt.show()

# min - max skaliranje
sc = StandardScaler ()
X_train_n = sc . fit_transform ( X_train )

df1 = pd.DataFrame(X_train)
df2 = pd.DataFrame(X_train_n)

plt.hist(X_train, bins=5)
plt.show()
plt.hist(X_train_n, bins=5)
plt.show()

#df1. plot( kind ='hist', bins = 20 )
#plt . show ()
#df2. plot( kind ='hist', bins= 20)
#plt . show ()

linear_model.LinearRegression(*, fit_intercept = True , copy_X = True , n_jobs = None , positive = False )
regr = linear_model.LinearRegression()
