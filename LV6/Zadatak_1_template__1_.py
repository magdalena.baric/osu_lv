import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

from sklearn . model_selection import cross_val_score


def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                           np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("Social_Network_Ads.csv")
print(data.info())

data.hist()
plt.show()

# dataframe u numpy
X = data[["Age", "EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, stratify=y, random_state=10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# Model logisticke regresije
LogReg_model = LogisticRegression(penalty=None)
LogReg_model.fit(X_train_n, y_train)

# Evaluacija modela logisticke regresije
y_train_p = LogReg_model.predict(X_train_n)
y_test_p = LogReg_model.predict(X_test_n)

print("Logisticka regresija: ")
print("Tocnost train: " +
      "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p))))

# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=LogReg_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()


# 1.zad
KNN_model = KNeighborsClassifier(n_neighbors=5)


KNN_model.fit(X_train_n, y_train)
y_train_p_knn = KNN_model.predict(X_train_n)
y_test_p_knn = KNN_model.predict(X_test_n)
print("KNN:")
print("Tocnost train: " +
      "{:0.3f}".format((accuracy_score(y_train, y_train_p_knn))))
print("Tocnost test: " +
      "{:0.3f}".format((accuracy_score(y_test, y_test_p_knn))))
# Ovi podaci su tocniji u usporedbi sa logistickom regresijom


plot_decision_regions(X_train_n, y_train, classifier=KNN_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost(knn-train): " +
          "{:0.3f}".format((accuracy_score(y_train, y_train_p_knn))))
plt.tight_layout()
plt.show()
# K=1--> overfit, K=100--> underfit

# 2.zad
KNN_model2 = KNeighborsClassifier()
param_grid = {'n_neighbors': np.arange(1, 100)}
KNN_gs = GridSearchCV(KNN_model2, param_grid, cv=5)
KNN_gs.fit(X_train_n, y_train)

print(KNN_gs.best_params_, KNN_gs.best_score_)

# za K=5
cv_scores = cross_val_score(KNN_model, X_train_n, y_train, cv=5)
print('cv_scores.mean()', format(np.mean((cv_scores))))

# 3.zad
SVM_model = svm.SVC(kernel='rbf', gamma=1, C=0.1)


SVM_model.fit(X_train_n, y_train)
y_train_p_svm = SVM_model.predict(X_train_n)
y_test_p_svm = SVM_model.predict(X_test_n)
print("KNN:")
print("Tocnost train: " +
      "{:0.3f}".format((accuracy_score(y_train, y_train_p_svm))))
print("Tocnost test: " +
      "{:0.3f}".format((accuracy_score(y_test, y_test_p_svm))))


plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost(knn-train): " +
          "{:0.3f}".format((accuracy_score(y_train, y_train_p_svm))))
plt.tight_layout()
plt.show()


# 4.zad

param_grid_svm = {'C': [1, 5, 10, 20, 30], 'gamma': [0.01, 0.1, 1, 5, 10, 15]}

svm_grid = GridSearchCV(svm.SVC(), param_grid_svm, cv=5, scoring='accuracy')
svm_grid.fit(X_train_n, y_train)
scores_svm = svm_grid.cv_results_
print(

    f'Rezultati GridSearcha za SVM za dane parametre:\n{pd.DataFrame(scores_svm)}')
print(
    f'Optimalni parametri su {svm_grid.best_params_} uz prosječnu točnost od {svm_grid.best_score_} (SVM)')
