fspam=open('SMSSpamCollection.txt',encoding="utf8")
ham_num=0
spam_num=0
ham_sum=0
spam_sum=0
spam_exc=0

for line in fspam:
    line=line.strip()

    if line.startswith('ham'):
        ham_num=ham_num + 1                     #broji koliko ima ham redaka
        ham_sum=ham_sum + len(line.split())     #broji duljinu linije, tj. koliko rijeci ima u liniji
    elif line.startswith('spam'):
        spam_num=spam_num + 1
        spam_sum=spam_sum + len(line.split())

        if line.endswith('!'):
            spam_exc =spam_exc+ 1               #broji koliko spam poruka zavrsava usklicnikom


print("Prosjecan broj rijeci u SMS porukama koji su tipa spam iznosi: ", spam_sum/spam_num)
print("Prosjecan broj rijeci u SMS porukama koji su tipa ham iznosi: ", ham_sum/ham_num )
print("Ukupan broj SMS poruka koje zavrsavaju usklicnikom, a koje su tipa spam iznosi: ", spam_exc)
