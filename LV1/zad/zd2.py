try:
    broj=float(input('Unesi broj: '))
    
except:
    print('Pogresan unos!')

while broj<0.0 or broj>1.0:
    print('Ponovno unesite broj u intervalu [0.0, 1.0]: ')
    broj=float(input())


if broj<0.0 and broj>1.0:
    print('Nevazeci interval')
if broj>=0.9:
    print('A')
elif broj>=0.8:
    print('B')
elif broj>=0.7:
    print('C')
elif broj>=0.6:
    print('D')
elif broj<0.6:
    print('F')
else:
    print()