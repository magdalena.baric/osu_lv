song_file=open('song.txt')
song_dict = dict()
counter=0 

for line in song_file:
    line=line.strip()               #uklanja razmake s pocetka i kraja niza
    line=line.lower()               #pretvaranje u mala slova
    line=line.replace(",", "")      #zamjena svih zareza
    words=line.split(" ")           #razdvoji sve rijeci izmedu kojih je razmak(sve rijeci)

    for word in words:
        if word in song_dict:
            song_dict[word] = song_dict[word] + 1   #broji koliko ce se puta pojaviti neka rijec
        else:
            song_dict[word] = 1          #ako je prva pojava brojac se postavlja na 1

for key in list(song_dict.keys()):
        print(key, ":", song_dict[key])
        if song_dict[key] == 1:
            counter=counter + 1            #broji rijeci koje se samo 1 pojavljuju

print("Rijeci koje se ponavljaju samo jednom je ", counter, ", a to su:") 

for key in list(song_dict.keys()):          #ispis
       if song_dict[key] == 1:
            counter=counter + 1
            print(key, ":", song_dict[key])
