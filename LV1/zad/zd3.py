mylist=[]
sum=0

while True:
    try: 
        input_simbol=input('Unesite broj: ')
        if input_simbol=='Done':
            break
        mylist.append(float(input_simbol))      #ako ne stavim float nece prepoznat i ispisat except
        sum+=float(input_simbol)
    except:
        print('Pogresan unos! ')
    
print('Korisnik je unio ', len(mylist), 'broja')
print('Srednja vrijednost je: ', sum/len(mylist) )
print('Minimum: ', min(mylist))
print('Maksimum: ', max(mylist))

mylist.sort()
print('sortirana lista: 'mylist)
