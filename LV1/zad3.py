list=[]
sum=0
while True:
    try:
        print('Unesite broj: ')
        input_simbol =input()
        if input_simbol == 'Done':
            break
        list.append(float(input_simbol))
    except:
        print("Pogresan unos, ponovite")

for input_simbol in range(len(list)):
    sum = sum+int(list[input_simbol])
  
print('Korisnik je unio ', len(list), 'broja')
print('Srednja vrijednost liste je: ', sum/len(list))
print('Minimum od liste je ', min(list))
print('Maksimum od liste je ', max(list))

print('Izvorna lista:  ')
print(list)

print('Sortirana lista: ')
list.sort()
print(list)
