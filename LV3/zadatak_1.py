import pandas as pd
import numpy as np

data = pd . read_csv('data_C02_emission.csv')
print("DataFrame sadrzi", len(data), "mjerenja")

print("Tipovi velicina: ")
print(data.dtypes)


dupl = data.duplicated().sum()
print(dupl)

nu = data.isnull().sum().sum()

if dupl == 0:
    print("Nema dupliciranih vrijednosti")
else:
    print("ima dupliciranih vrijednosti")
    data.drop_duplicates()

if nu == 0:
    print("Nema praznih vrijednosti")
else:
    print("Ima praznih vrijednosti")
    data.dropna(axis=1)
    data.dropna(axis=0)
    data = data.reset_index(drop=True)


data = data.reset_index(drop=True)
print(data.dtypes)

# data["Make", "Model", "Vehicle class", "Transmission",
#    "Fuel type"] = data["Make", "Model", "Vehicle class", "Transmission",
#                       "Fuel type"].astype('category')

for col in ["Make", "Model", "Vehicle Class", "Transmission", "Fuel Type"]:
    data[col] = data[col].astype('category')

print("Pretvorene vrijednosti:")
print(data.dtypes)


# sorted_data = data.sort_values(
#   by=['Fuel Consumption City (L/100km)'], ascending=True)
print("prva 3 min")
sort_data = data.sort_values(
    by=['Fuel Consumption City (L/100km)'], ascending=True)
print(sort_data.iloc[0:3, [0, 1, 7]])

print("prva 3 max")
sort_data = data.sort_values(
    by=['Fuel Consumption City (L/100km)'], ascending=False)
print(sort_data.iloc[0:3, [0, 1, 7]])


engine_data = data[(data['Engine Size (L)'] > 2.5) &
                   (data['Engine Size (L)'] < 3.5)]
print("U rasponu je", len(engine_data), "automobila")

print("Prosjecna CO2 emisija za ova vozila je: ",
      engine_data['CO2 Emissions (g/km)'].mean())
