import pandas as pd

data = pd . read_csv ('data_C02_emission.csv')
print('Broj mjerenja: ', len(data))             #broj redaka predstavlja broj mjerenja
print('Tipovi velicina:\n', data.dtypes)        #dtypes funkcija

#ima jedan nacin i u predlosku

dupl = data.duplicated().sum()
print(dupl)
nu = data.isnull().sum().sum()
if dupl == 0:
    print("Nema dupliciranih vrijednosti")
else:
    print("ima dupliciranih vrijednosti")
    data.drop_duplicates()

if nu == 0:
    print("Nema praznih vrijednosti")
else:
    print("Ima praznih vrijednosti")
    data.dropna(axis=1)
    data.dropna(axis=0)
    data = data.reset_index(drop=True)
data = data.reset_index(drop=True)
print(data.dtypes)

#pretvaranje object u categoric
list_str_obj_cols = data.columns[data.dtypes == "object"].tolist()
for str_obj_col in list_str_obj_cols:
    data[str_obj_col] = data[str_obj_col].astype("category")
print(data.dtypes)

#print(f'Broj mjerenja: {len(data)}')
#print('Tipovi velicina:')
#print(data.dtypes)
#print(f'Broj dupliciranih vrijednosti:{data.duplicated().sum()}')
#print('Broj izostalih vrijednosti po stupcima:')
# print(data.isnull().sum())
# data['Make']=pd.Categorical(data['Make'])
# data['Vehicle Class']=pd.Categorical(data['Vehicle Class'])
# data['Transmission']=pd.Categorical(data['Transmission'])
# data['Fuel Type']=pd.Categorical(data['Fuel Type'])
# print(data.dtypes)


#b)
data_3= data.sort_values('Fuel Consumption City (L/100km)', ascending=True)
print(data_3.head(3).iloc[: , [0, 1, 7]])
#topAndBottom3=data.sort_values(by='Fuel Consumption City (L/100km)')
#print('Najmanja 3:')
# #print(topAndBottom3[['Make','Model','Fuel Consumption City (L/100km)']].head(3))
#print('Najveća 3:')
#print(topAndBottom3[['Make','Model','Fuel Consumption City (L/100km)']].tail(3))

#c
engine_data = data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)]
print("U rasponu je", len(engine_data), "automobila")
print("Prosjecna CO2 emisija za ova vozila je: ", engine_data['CO2 Emissions (g/km)'].mean())
#print(data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)].agg({'Model':'count', 'CO2 Emissions (g/km)':'mean'}))


#d
print(f"Broj Audija:{len(data[data['Make']=='Audi'])}")
print(f"Prosjecna emisija CO2 audija s 4 cilindra: {(data[(data['Make']=='Audi') & (data['Cylinders']==4)])['CO2 Emissions (g/km)'].mean()} (g/km)")

#e
data = pd . read_csv ('data_C02_emission.csv')
new_data = data.groupby ('Cylinders')
print ( new_data.count () )
print ( new_data['CO2 Emissions (g/km)'].mean())
#print(data.groupby('Cylinders').agg({'Make':'count', 'CO2 Emissions (g/km)':'mean'}).rename(columns={'Make': 'Car count'}))

#f
print('Mean i median za dizel:')
print(data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].agg(['mean','median']))
print('Mean i median za benzin:')
print(data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].agg(['mean','median']))

#g
print('Vozilo s 4 cilindra dizel s najvećom potrošnjom u gradu:')
print(data[(data['Cylinders']==4) & (data['Fuel Type']=='D')].sort_values(by='Fuel Consumption City (L/100km)').tail(1))

#h
print(f"Broj vozila s manualnim mjenjačem: {len(data[data['Transmission'].str.startswith('M')])}")

#i
print(data.corr(numeric_only=True))
