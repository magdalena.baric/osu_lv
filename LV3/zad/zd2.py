import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

data = pd . read_csv ('data_C02_emission.csv')

#a
data ['CO2 Emissions (g/km)']. plot (kind='hist', bins = 20 )
plt.show()

#b
data['Fuel Type']=pd.Categorical(data['Fuel Type'])     #moram pretvorit u kategoricku jer nece htjet napravit scater
data . plot . scatter ( x='Fuel Consumption City (L/100km)' ,
                        y='CO2 Emissions (g/km)' ,
                        c='Fuel Type', cmap ="viridis", s=50 )
plt.show()

#c
data.groupby('Fuel Type').boxplot(column='Fuel Consumption Hwy (L/100km)')
plt.show()

#d
data_g= data.groupby('Fuel Type')
data_g['Make'].count().plot(kind='bar')
plt.show()
#data.groupby('Fuel Type').agg({'Make':'count'}).rename(columns={'Make':'Car number'}).plot(kind="bar")
#plt.show()

#e
data_g= data.groupby('Cylinders')
data_g['CO2 Emissions (g/km)'].mean().plot(kind='bar')
plt.show()

