import numpy as np
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
# pick a sample to plot
sample = 1
image = x_train[sample]
# plot the sample
fig = plt.figure
plt.imshow(image, cmap='gray')
plt.title('Oznaka: {}'.format(y_train[1]))
plt.show()


sample = 5
image = x_train[sample]
# plot the sample
fig = plt.figure
plt.imshow(image, cmap='gray')
plt.title('Oznaka: {}'.format(y_train[5]))
plt.show()


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras . Sequential(
    [
        layers.Flatten(input_shape=(28, 28, 1)),
        layers.Dense(128, activation="relu"),
        layers.Dense(64, activation="relu"),
        layers.Dense(10, activation="softmax"),
    ]

    #model . add(layers.Input(shape=(784, )))
    #model . add(layers.Dense(4, activation="relu"))
    #model . add(layers.Dense(3, activation="relu"))
    #model . add(layers.Dense(2, activation="softmax"))
)
model . summary()


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model . compile(loss="categorical_crossentropy",
                optimizer="adam",
                metrics=["accuracy", ])

# TODO: provedi ucenje mreze
x_train = x_train.reshape(-1, 28, 28, 1)
x_test = x_test.reshape(-1, 28, 28, 1)


# Convert the labels to categorical one-hot encoding
y_train = keras.utils.to_categorical(y_train, num_classes=10)
y_test = keras.utils.to_categorical(y_test, num_classes=10)

history = model.fit(
    x_train,
    y_train,
    batch_size=32,
    epochs=5,
    validation_data=(x_test, y_test),
)

# Print the history of the training process
print(history.history)

# TODO: Prikazi test accuracy i matricu zabune
score = model . evaluate(x_test, y_test, verbose=0)
print('Evaluacija: ', score)

# Get the model's predictions for the test data
y_pred = model.predict(x_test_s)

# Convert the predictions to class labels
y_pred_labels = np.argmax(y_pred, axis=1)

# Get the true class labels for the test data
y_true_labels = np.argmax(y_test_s, axis=1)

# Compute the confusion matrix
cm = confusion_matrix(y_true_labels, y_pred_labels)

# Display the confusion matrix
print('Confusion matrix:\n', cm)


# TODO: spremi model
model.save('model_mb')
