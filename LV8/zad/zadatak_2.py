import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from keras.models import load_model
import random

num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255


# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)


# Reshaped arrays from (60000, 28, 28, 1) to (60000, 784)
x_train_s=x_train_s.reshape(x_train_s.shape[0],-1)
x_test_s=x_test_s.reshape(x_test_s.shape[0],-1)


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# Ucitavanje modela
model= load_model('ImageReader/')
predictions=model.predict(x_test_s)


# Pretvaranje podataka nazad u brojeve
predictions=(predictions >=0.5).astype(int)
y_test_s=y_test_s.astype(int)
y_pred=np.argmax(predictions, axis=1)
y_true=np.argmax(y_test_s, axis=1)


# Pronalazenje lose klasificiranih podataka
miss_index_list=[]
for i in range(x_test_s.shape[0]):
    if y_pred[i]!=y_true[i]:
        miss_index_list.append(i)


# Crtanje lose klasificiranih podataka
for _ in range(3):
    plt.figure()
    i=random.choice(miss_index_list)
    plt.imshow(x_test[i,:,:])
    plt.title(f'Predicted number: {y_pred[i]}       Real number: {y_true[i]}')
    plt.show()


'''#
#ucitavanje modela
model = load_model('Model/')
model.summary()
(X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()
X_test_reshaped = np.reshape(X_test,(len(X_test),X_test.shape[1]*X_test.shape[2])) #za predikciju

#predikcija, za prikaz lose klasificiranih
y_predictions = model.predict(X_test_reshaped) 
y_predictions = np.argmax(y_predictions, axis=1)

#prikaz nekih krivih predikcija
wrong_predictions = y_predictions[y_predictions != y_test]   #krive predikcije modela
wrong_predictions_correct = y_test[y_predictions != y_test]  #ispravke krivih predikcija (koje je model promasio i stavio krive)
images_wrong_predicted = X_test[y_predictions != y_test]     #slike se prikazuju 2d poljem, ne 1d
fig, axs = plt.subplots(2,3, figsize=(12,9))
br=0 #brojac za prikaz slike
for i in range(2):
    for j in range(3):
        axs[i,j].imshow(images_wrong_predicted[br])
        axs[i,j].set_title(f'Model predvidio {wrong_predictions[br]}, zapravo je {wrong_predictions_correct[br]}')
        br=br+1
plt.show()

'''
